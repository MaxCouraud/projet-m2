<?php

namespace App\Controller;

use App\Impl\IMotion;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MotionController extends AbstractController implements IMotion
{


    private $newTargetLigne = 1;
    private $newTargetColonne = 2;
    private $newPositionLigne = 1;
    private $newPositionColonne = 1;
    private $table;

    /**
     * @Route("/", name="motion")
     */
    public function index(): Response
    {
        $this->getInitialTable();
        $initialTable = $this->table;
        //ajouté des valeurs de test
        $this->table[1][5] =-1;
        foreach($initialTable as $item)
        {
            foreach($item as $value)
            {
                if($this->scan($this->newTargetLigne, $this->newTargetColonne)) {
                    $initialTable[$this->newTargetLigne][$this->newTargetColonne] = 1;
                    $this->move();
                }else{
                    $this->rotate($this->newTargetLigne, $this->newTargetColonne, $this->newPositionLigne,$this->newPositionLigne);
                }
            }
        }

        return $this->render('motion/index.html.twig', [
            'initialTable' => $initialTable,
        ]);
    }

    private function getInitialTable()
    {
        $ligne = array_fill(0, 66, 0);
        $this->table = array_fill(0, 66, $ligne);
        $this->table[0] = array_fill( 0, 66, -1);
        $this->table[65] = array_fill( 0, 66, -1);

        for($i=0; $i<66; $i++)
        {
            $this->table[$i][0] = -1;
            $this->table[$i][65] = -1;
        }
        return $this->table;
    }


    public function scan($ligneTarget, $colonneTarget)
    {
        if($this->table[$ligneTarget][$colonneTarget] == 0)
        {
            return true;
        }else{
            return false;
        }
    }

    public function rotate($targetLigne, $targetColonne, $ligne, $colonne)
    {
        //si l'ancienne target était à droite alors ce sera celle du bas
        if($lastTargetLigne = $ligne && $lastTargetColonne = $colonne+1)
        {
            $this->newTargetLigne = $lastTargetLigne +1;
            $this->newTargetColonne = $lastTargetColonne -1;
        }
        //si l'ancienne target était celle en bas alors ce sera celle a gauche
        if($lastTargetLigne = $ligne+1 && $lastTargetColonne = $colonne)
        {
            $this->newTargetLigne = $lastTargetLigne -1;
            $this->newTargetColonne = $lastTargetColonne -1;
        }
        //si l'ancienne target était celle a gauche alors ce sera celle du haut
        if($lastTargetLigne = $ligne && $lastTargetColonne = $colonne-1)
        {
            $this->newTargetLigne = $lastTargetLigne -1;
            $this->newTargetColonne = $lastTargetColonne +1;
        }
        //si l'ancienne target était celle en haut alors ce sera celle à droite
        if($lastTargetLigne = $ligne-1 && $lastTargetColonne = $colonne)
        {
            $this->newTargetLigne = $lastTargetLigne -1;
            $this->newTargetColonne = $lastTargetColonne +1;
        }

    }

    public function move()
    {
        $this->newPositionColonne = $this->newTargetColonne;
        $this->newPositionLigne = $this->newTargetLigne;
        $this->newTargetColonne =  $this->newTargetColonne +1;
    }

    private function getOnePlace($ligne, $colonne)
    {
        $table = $this->getInitialTable();

        return $table[$ligne][$colonne];
    }

}
