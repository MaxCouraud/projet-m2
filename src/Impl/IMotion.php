<?php


namespace App\Impl;


interface IMotion
{
    public function rotate($lastTargetLigne, $lastTargetColonne, $ligne, $colonne);
    public function move();
}
